@echo off 
echo ---------------------------------------
echo --- Vypnuti pocitace 0.10 alpha -------
echo ---------------------------------------
echo Pro zruseni vypnuti pocitace zadejte  
echo cislo mensi nebo rovno 0
echo ---------------------------------------

:setTime
SET /P ANSWER=Zadejte cas v minutach: 

SET "var="&for /f "delims=0123456789" %%i in ("%ANSWER%") do set var=%%i

if defined var (
	echo Zadana hodnota musi byt cislo, zkuste to prosim znovu
	goto :setTime
)

SET @minutes=%ANSWER%
SET /A @seconds=%@minutes%*60

if /I %@minutes% LEQ 0 (
	shutdown -a
	echo Naplanovane vypnuti bylo zruseno
)

if /I %@minutes% GTR 0 (
	shutdown -s -f -t %@seconds%
	echo Pocitac se vypne za %@minutes% minut
)